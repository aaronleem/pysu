# pysu: the python osu api library
# Copyright (C) 2018 Aaron Marais

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .constants import Constants
from types import SimpleNamespace as SN
import datetime


class SimpleNamespace(SN):
    def get(self, i):
        return self.__dict__[i]


class Score:
    def __init__(self, data):
        self.score = int(data["score"])
        self.userid = int(data["user_id"])
        self.pp = float(data.get("pp", 0))
        self.beatmap_id = int(data.get("beatmap_id"))
        self.maxCombo = int(data["maxcombo"])
        self.perfect = data["perfect"] == '1'
        self.date = data["date"]
        self.rank = data["rank"]

        self.raw_mods = int(data["enabled_mods"])

        self.count = SimpleNamespace(**{
            '300':  int(data["count300"]),
            '100':  int(data["count100"]),
            '50':   int(data["count50"]),
            'geki': int(data["countgeki"]),
            'katu': int(data["countkatu"]),
            'miss': int(data["countmiss"])
        })

    @property
    def mods(self):
        return ", ".join(
            Constants.NamedMods[m.name]
            for m in Constants.Mods
            if self.raw_mods & m.value and m.name in Constants.NamedMods
        ) or "None"

    @property
    def accuracy(self):
        player_sum = self.count.get("50") * 50 + self.count.get("100") * 100 + self.count.get("300") * 300
        map_sum = (self.count.get("miss") + self.count.get("50") + self.count.get("100") + self.count.get("300")) * 300
        acc = player_sum / map_sum

        return "%s%%" % round(acc * 100, 2)

