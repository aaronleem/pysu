# pysu: the python osu api library
# Copyright (C) 2018 Aaron Marais

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from types import SimpleNamespace
from .event import Event


class User:
    def __init__(self, data):
        self.id = data["user_id"]
        self.name = data["username"]
        self.country = data["country"]
        self.level = float(data["level"] or 0)
        self.accuracy = float(data["accuracy"] or 0)
        self.accuracy_formatted = "%.2f%%" % self.accuracy

        self.count = {
            '300': int(data["count300"] or 0),
            '100': int(data["count100"] or 0),
            '50': int(data["count50"] or 0),
            'SS': int(data["count_rank_ss"] or 0),
            'S': int(data["count_rank_s"] or 0),
            'A': int(data["count_rank_a"] or 0),
            'plays': int(data["playcount"] or 0)
        }
        self.score = SimpleNamespace(
            ranked=int(data["ranked_score"] or 0),
            total=int(data["total_score"] or 0)
        )
        self.pp = SimpleNamespace(
            raw=float(data["pp_raw"] or 0),
            rank=int(data["pp_rank"] or 0),
            country_rank=int(data["pp_country_rank"] or 0)
        )

        if data.get("events"):
            self.events = map(lambda ev: Event(ev), data["events"])
        else:
            self.events = None
