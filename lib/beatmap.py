# pysu: the python osu api library
# Copyright (C) 2018 Aaron Marais

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from .constants import Constants
from types import SimpleNamespace
import datetime


class Beatmap:
    def __init__(self, data):
        self.id = int(data["beatmap_id"])
        self.set_id = int(data["beatmapset_id"])
        self.hash = data["file_md5"]
        self.title = data["title"]
        self.creator = data["creator"]
        self.version = data["version"]

        self.source = data["source"]
        self.artist = data["artist"]
        self.genre = Constants.Beatmaps.genre[int(data["genre_id"])]
        self.language = Constants.Beatmaps.language[int(data["language_id"])]

        self.bpm = float(data["bpm"])
        self.mode = Constants.Beatmaps.mode[int(data["mode"])]
        self.tags = data["tags"].split(' ')
        self.approval_status = Constants.Beatmaps.approved[int(data["approved"])]
        self.approved_date = data["approved_date"]
        self.raw_lastUpdate = data["last_update"]
        self.max_combo = data["max_combo"]
        self.difficulty = SimpleNamespace(
            rating=data["difficultyrating"],
            size=data["diff_size"],
            overall=data["diff_overall"],
            approach=data["diff_approach"],
            drain=data["diff_drain"]
        )
        self.time = SimpleNamespace(
            total=int(data["total_length"]),
            drain=int(data["hit_length"])
        )
        self.counts = SimpleNamespace(
            favorites=int(data["favourite_count"]),
            plays=int(data["playcount"]),
            passes=int(data["passcount"])
        )

    @property
    def last_update(self):
        if hasattr(self, '_lastUpdate'):
            return self._lastUpdate

        # UTC+8 so we subtract 8 hours to get the UTC time
        millis = 1288483950000
        ts = millis * 1e-3
        local_dt = datetime.fromtimestamp(ts, get_localzone())
        utc_offset = local_dt.utcoffset()

        self._lastUpdate = datetime.fromtimestamp(parse(self.raw_lastUpdate) - 28800000 - (utc_offset * 60000))
        return self._lastUpdate



