# pysu: the python osu api library
# Copyright (C) 2018 Aaron Marais

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum


class Constants:
    NamedMods = {
        "NoFail": "No Fail", "Easy": "Easy", "Hidden": "Hidden", "HardRock": "Hard Rock", 
        "SuddenDeath": "Sudden Death", "DoubleTime": "Double Time", "Relax": "Relax", 
        "HalfTime": "Half Time", "Nightcore": "Nightcore", "Flashlight": "Flashlight", 
        "Autoplay": "Autoplay", "SpunOut": "Spun Out", "Autopilot": "Autopilot", 
        "Perfect": "Perfect", "Key1": "1k", "Key2": "2k", "Key3": "3k", "Key4": "4k", "Key5": "5k", 
        "Key6": "6k", "Key7": "7k", "Key8": "8k", "Key9": "9k", "Key10": "10k", 
        "keyMod": "keyMod", "FadeIn": "Fade In", "Random": "Random"
    }

    class Mods(Enum):
        NoFail = 1
        Easy = 1 << 1
        NoVideo = 1 << 2
        Hidden = 1 << 3
        HardRock = 1 << 4
        SuddenDeath = 1 << 5
        DoubleTime = 1 << 6
        Relax = 1 << 7
        HalfTime = 1 << 8
        Nightcore = 1 << 9
        Flashlight = 1 << 10
        Autoplay = 1 << 11
        SpunOut = 1 << 12
        Autopilot = 1 << 13
        Perfect = 1 << 14
        Key4 = 1 << 15
        Key5 = 1 << 16
        Key6 = 1 << 17
        Key7 = 1 << 18
        Key8 = 1 << 19
        FadeIn = 1 << 20
        Random = 1 << 21
        Cinema = 1 << 22
        Target = 1 << 23
        Key9 = 1 << 24
        KeyCoop = 1 << 25
        Key10 = 1 << 26
        Key1 = 1 << 27
        Key3 = 1 << 28
        Key2 = 1 << 29
        LastMod = 1 << 30

        KeyMod = Key1 | Key2 | Key3 | Key4 | Key5 | Key6 | Key7 | Key8 | Key9 | KeyCoop
        FreeModAllowed = NoFail | Easy | Hidden | HardRock | SuddenDeath | Flashlight | FadeIn |\
                         Relax | Autopilot | SpunOut | KeyMod
        ScoreIncreaseMods = Hidden | HardRock | DoubleTime | Flashlight | FadeIn

    class Beatmaps:
        approved = [
            "Pending",
            "Ranked",
            "Approved",
            "Qualified",
            "Loved",
            "WIP",
            "Graveyard"
        ]
        genre = [
            "Any",
            "Unspecified",
            "Video Game",
            "Anime",
            "Rock",
            "Pop",
            "Other",
            "Novelty",
            "",
            "Hip Hop",
            "Electronic"
        ]
        language = [
            "Any",
            "Other",
            "English",
            "Japanese",
            "Chinese",
            "Instrumental",
            "Korean",
            "French",
            "German",
            "Swedish",
            "Spanish",
            "Italian"
        ]
        mode = [
            "Standard",
            "Taiko",
            "Catch the Beat",
            "Mania"
        ]

    class Multiplayer:
        scoringType = [
            "Score",
            "Accuracy",
            "Combo",
            "Score v2"
        ]
        teamType = [
            "Head to Head",
            "Tag Co-op",
            "Team vs",
            "Tag Team vs"
        ]
        team = [
            "None",
            "Blue",
            "Red"
        ]

    class URLSchemas:
        multiplayer_match = lambda id, password: f"osu://mp/${id}${('/' + password) if password is not None else ''}"
        edit = lambda position, objects: f"osu://edit/${position}${(' ' + objects) if objects is not None else ''}"
        channel = lambda name: f"osu://chan/#${name}"
        download = lambda id: f"osu://dl/${id}"
        spectate = lambda user: f"osu://spectate/${user}"

