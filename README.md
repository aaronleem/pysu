# Psyu
### The Python osu API library

## What is osu?
osu is a rhythm game in which you use your mouse (or graphics tablet) and keyboard to click on circles, sliders and spinners in order to complete levels

## Usage
### osu.api(key="", api_url='https://osu.ppy.sh/api', raise_errors=False, completeScores=False)

	key: your API key
	api_url: the base URL to the API
	raise_errors: raise errors as Exceptions instead of sending no data
	completeScores: unused as of yet

Returns: Class instance

#### api.get_user(uid, params={})

	uid: the user ID or name you wish to check
	params: any extra parameters you require

Returns: [User class](#User)

#### api.get_user_recent(uid, params={})

	uid: the user ID or name you wish to check
	params: any extra parameters you require

Returns: List of [Score class](#Score) instances

#### api.get_user_best(uid, params={})

	uid: the user ID or name you wish to check
	params: any extra parameters you require

Returns: List of [Score class](#Score) instances

#### api.get_beatmaps(bmid, params={})

	bmid: the beatmap or beatmap set ID you wish to retrieve
	params: any extra parameters you require

Returns: List of [Beatmap class](#Beatmap) instances

#### api.get_beatmap(bmid, params={})

	bmid: the beatmap ID you wish to retrieve
	params: any extra parameters you require

Returns: Single [Beatmap class](#Beatmap) instance

## Class reference

### User

#### User.id

	Type: int
	Contains: User ID

#### User.name

	Type: str
	Contains: User name

#### User.country

	Type: str
	Contains: User's country

#### User.level

	Type: float
	Contains: User's level

#### User.accuracy

	Type: Float
	Contains: User's average accuracy

#### User.accuracy_formatted

	Type: str
	Contains: User's average accuracy formatted with 2 decimal numbers with a percentage symbol

#### User.count

	Type: SimpleNamespace
	Contains: User's different total counts

#### User.count['300']

	Type: int
	Contains: User's total on-beat hits

#### User.count['100']

	Type: int
	Contains: User's slightly off-beat hits

#### User.count['50']

	Type: int
	Contains: User's very off-beat hits

#### User.count.SS

	Type: int
	Contains: User's total full-cleared on-beat maps

#### User.count.S

	Type: int
	Contains: User's total full-cleared maps which have had one or more notes off-beat

#### User.count.A

	Type: int
	Contains: User's total maps which have been completed with either a low amount of on-beat hits or at least one note missed

#### User.count.plays

	Type: int
	Contains: User's total amount of times played

#### User.score

	Type: SimpleNamespace
	Contains: User's ranked and total score

#### User.score.ranked

	Type: int
	Contains: User's total ranked score

#### User.score.total

	Type: int
	Contains: User's total score across all maps

#### User.pp

	Type: SimpleNamespace
	Contains: Information of user's Performance Points

#### User.pp.raw

	Type: int
	Contains: User's total PP

#### User.pp.rank

	Type: int
	Contains: User's global rank

#### User.pp.country_rank

	Type: int
	Contains: User's country rank

#### User.events

	Type: list
	Contains: List of (Events)[#Event] the user's part of

### Beatmap

#### Beatmap.id

	Type: int
	Contains: The singular beatmap's ID

#### Beatmap.set_id

	Type: int
	Contains: The ID of the set the beatmap belongs to

#### Beatmap.hash

	Type: str
	Contains: The beatmap's hashsum

#### Beatmap.title

	Type: str
	Contains: The title of the beatmap

#### Beatmap.creator

	Type: str
	Contains: The user that created the beatmap

#### Beatmap.version

	Type: str
	Contains: The version of the map (difficulty)

#### Beatmap.source

	Type: str
	Contains: Honestly.. no idea

#### Beatmap.artist

	Type: str
	Contains: The creator of the beatmap's song

#### Beatmap.genre

	Type: str
	Contains: The genre of the song

#### Beatmap.language

	Type: str
	Contains: The language of the song

#### Beatmap.bpm

	Type: int
	Contains: The amount of beats per minute of the song

#### Beatmap.mode

	Type: str
	Contains: The game mode which this beatmap was made for

#### Beatmap.tags

	Type: list(str)
	Contains: A list of tags which the beatmap was submitted with

#### Beatmap.approval_status

	Type: str
	Contains: The ranked approval status of the map

#### Beatmap.raw_approvedDate

	Type: str
	Contains: If the map was approved, this is the date it was approved

#### Beatmap.raw_lastUpdate

	Type: str
	Contains: The last update to the beatmap

#### Beatmap.max_combo

	Type: int
	Contains: The maximum possible combo of the beatmap

#### Beatmap.difficulty

	Type: SimpleNamespace
	Contains: Information of this beatmap's difficulty

#### Beatmap.time

	Type: SimpleNamespace
	Contains: The total and drain length of this beatmap

#### Beatmap.counts

	Type: SimpleNamespace
	Contains: The amount of favorites, total plays and total passes of this beatmap

#### Beatmap.counts.favorites

	Type: int
	Contains: The amount of favorites of this beatmap

#### Beatmap.counts.plays

	Type: int
	Contains: The amount of total plays of this beatmap

#### Beatmap.counts.passes

	Type: int
	Contains: The amount of total passes of this beatmap

#### Beatmap.approved_date

	Type: read-only property (datetime)
	Contains: Time-formatted property which displays when the beatmap was approved

#### Beatmap.last_update

	Type: read-only property (datetime)
	Contains: Time-formatted property which displays when the beatmap was last updated

### Score

#### Score.score

	Type: int
	Contains: Your max score attained on this play

#### Score.userid

	Type: int
	Contains: The ID of the user that made this play

#### Score.pp

	Type: float
	Contains: The total PP gained from this play

#### Score.beatmap_id

	Type: int
	Contains: The ID for the beatmap this play was made on

#### Score.maxCombo

	Type: int
	Contains: The maximum combo attained for this play

#### Score.perfect

	Type: bool
	Contains: All notes hit on-beat

#### Score.raw_date

	Type: str
	Contains: The raw date from the API

#### Score.rank

	Type: str
	Contains: The rank achieved on this play

#### Score.raw_mods

	Type: int
	Contains: The raw enabled mods from the API

#### Score.count

	Type: SimpleNamespace
	Contains: The amount of hits per type of hit

#### Score.count['300']

	Type: int
	Contains: The amount of on-beat hits

#### Score.count['100']

	Type: int
	Contains: The amount of slightly off-beat hit

#### Score.count['50']

	Type: int
	Contains: The amount of quite off-beat hits

#### Score.count.geki

	Type: int
	Contains: The amount of combos completed perfectly

#### Score.count.katu

	Type: int
	Contains: The amount of combos completed with at least one 100

#### Score.count.miss

	Type: int
	Contains: The amount of beats completely missed

#### Score.date

	Type: read-only property (datetime)
	Contains: The date this play was made

#### Score.mods

	Type: read-only property (str)
	Contains: A comma-separated list of mods which were enabled for this play

#### Score.accuracy

	Type: read-only property (str)
	Contains: A formatted display of the player's accuracy for this map

### Event

#### Event.html

	Type: str
	Contains: The HTML content for the event

#### Event.beatmap_id

	Type: int
	Contains: The beatmap ID for this event

#### Event.beatmapset_id

	Type: int
	Contains: The beatmap set ID for this event

#### Event.raw_date

	Type: str
	Contains: The raw date for this event

#### Event.epic_factor

	Type: str
	Contains: The epic factor for this event

#### Event.date

	Type: read-only property (datetime)
	Contains: The date of the event
