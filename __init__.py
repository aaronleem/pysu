# pysu: the python pysu api library
# Copyright (C) 2018 Aaron Marais

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import aiohttp
import async_timeout

from .lib.event import Event
from .lib.user import User
from .lib.beatmap import Beatmap
from .lib.score import Score


class modes:
    osu = 0
    taiko = 1
    ctb = 2
    mania = 3


class PysuError(Exception):
    pass


class API:
    def __init__(self, key=" ", api_url='https://osu.ppy.sh/api', raise_errors=False, completeScores=False):
        self.key = key
        self.api_url = api_url
        self.raise_errors = raise_errors
        self.completeScores = completeScores

    async def api_call(self, endpoint, options):
        if not self.key:
            raise PysuError('api key not set')

        options["k"] = self.key
        async with aiohttp.ClientSession() as session:
            async with async_timeout.timeout(15):
                async with session.get("%s/%s" % (self.api_url, endpoint), params=options) as response:
                    if response.status != 200:
                        raise PysuError(f"HTTP {response.status}")
                    else:
                        return await response.json()

    async def get_user(self, uid, **kwargs):
        id_type = 'id' if type(uid) == "int" or (type(uid) == "str" and uid.isdigit()) else 'string'
        data = await self.api_call("get_user", {'u': uid, 'type': id_type, **kwargs})
        if not data:
            if self.raise_errors:
                raise PysuError(f"User {uid} does not exist.")
            else:
                return data
        return User(data[0])

    async def get_beatmaps(self, **kwargs):
        data = await self.api_call("get_beatmaps", {**kwargs})
        if not data:
            if self.raise_errors:
                raise PysuError(f"Beatmap ID is invalid")
            else:
                return data
        return [Beatmap(bm) for bm in data]

    async def get_beatmap(self, bmid, **kwargs):
        data = await self.get_beatmaps(b=bmid, **kwargs)
        return data and data[0] or data

    async def get_user_recent(self, uid, **kwargs):
        id_type = 'id' if type(uid) == "int" or (type(uid) == "str" and uid.isdigit()) else 'string'
        data = await self.api_call("get_user_recent", {'u': uid, 'type': id_type, **kwargs})
        if not data:
            if self.raise_errors:
                raise PysuError(f"User {uid} does not exist.")
            else:
                return data

        recent_list = []
        for sc in data:
            recent_list.append(Score(sc))

        return recent_list

    async def get_user_best(self, uid, **kwargs):
        id_type = 'id' if uid.isdigit() else 'string'
        data = await self.api_call("get_user_best", {'u': uid, 'type': id_type, **kwargs})
        if not data:
            if self.raise_errors:
                raise PysuError(f"User {uid} does not exist.")
            else:
                return data

        recent_list = []
        for sc in data:
            recent_list.append(Score(sc))

        return recent_list

